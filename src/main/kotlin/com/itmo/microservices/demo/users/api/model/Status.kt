package com.itmo.microservices.demo.users.api.model

enum class Status {
    ONLINE, OFFLINE
}