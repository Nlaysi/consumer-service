package com.itmo.microservices.demo.users.api.model

data class AuthenticationRequest(val token: String)
