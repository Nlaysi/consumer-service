package com.itmo.microservices.demo.utils

import org.springframework.kafka.annotation.KafkaListener
import org.springframework.messaging.handler.annotation.Payload
import org.springframework.stereotype.Service
import java.util.*

interface KafkaConsumerService {
    fun receive(message: String?)
}

@Service
class Consumer(val wikiService: WikiService) : KafkaConsumerService {
    @KafkaListener(topics = ["#{'\${app.kafka.consumer.topic}'.split(',')}"])
    override fun receive(@Payload message: String?) {
        if (message != null) {
            val arr = message.split(':').toTypedArray()
            wikiService.calculateWikiWayLength(UUID.fromString(arr[0]), arr[1], arr[2])
        }
    }
}