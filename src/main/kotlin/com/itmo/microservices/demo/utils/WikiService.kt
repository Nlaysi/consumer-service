package com.itmo.microservices.demo.utils

import com.itmo.microservices.demo.utils.entity.WikiInfo
import com.itmo.microservices.demo.utils.repos.WikiRepository
import org.springframework.stereotype.Service
import java.util.*

@Service
class WikiService(
    val repository: WikiRepository,
    val parserService: ParserService
) {
    fun calculateWikiWayLength(id: UUID, from: String, to: String) {
        val info = WikiInfo(id)
        info.length = parserService.parseWiki(from, to)
        repository.save(info)
    }
}