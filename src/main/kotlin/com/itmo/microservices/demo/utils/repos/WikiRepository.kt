package com.itmo.microservices.demo.utils.repos

import com.itmo.microservices.demo.utils.entity.WikiInfo
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface WikiRepository  : JpaRepository<WikiInfo, UUID> {
}