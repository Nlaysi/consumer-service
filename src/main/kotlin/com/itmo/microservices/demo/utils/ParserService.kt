package com.itmo.microservices.demo.utils

import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.springframework.stereotype.Service
import java.util.*


@Service
class ParserService {

    fun parseWiki(pageFrom: String, pageTo: String) : Int {
        val maxLength = 4
        var length = 1
        val queue = LinkedList<String>()
        val set = mutableSetOf("/wiki/$pageFrom")
        queue.add("/wiki/$pageFrom")

        while (length <= maxLength) {
            val q = LinkedList<String>()
            while (queue.isNotEmpty()) {

                println(queue.size)

                val link = queue.pop()
                println(link)
                try {
                    var doc: Document = Jsoup.connect("https://ru.wikipedia.org$link").timeout(1000).get()
                    val body = doc.getElementById("bodyContent")
                    val elems = body.select("a")
                    for (elem in elems) {
                        val title: String = elem.attr("title")

                        println(title)
                        if (title == pageTo) {
                            return length
                        } else {
                            if (!set.contains("/wiki/$title")) {
                                q.add("/wiki/$title")
                                set.add("/wiki/$title")
                            }
                        }

//                        val href: String = elem.attr("href").also { println(it) }
//                        if (href.startsWith("/wiki")) {
//                            q.add(href)
//                        }
                    }
                } catch (e: Exception) {

                }
            }
            queue.addAll(q)
            length++
            println(length)
        }

        return -1
    }

}